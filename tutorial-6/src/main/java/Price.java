public abstract class Price {
    abstract int getPriceCode();

    abstract double getThisAmount(int daysRented);

    public int getFrequentRenterPoints(int daysRented) {
        return 1;
    }
}

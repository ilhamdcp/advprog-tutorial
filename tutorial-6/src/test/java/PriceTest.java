import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class PriceTest {
    private Price newRelease;

    @Before
    public void setUp() {
        newRelease = new NewReleasePrice();
    }

    @Test
    public void testPriceCode() {
        assertEquals(newRelease.getPriceCode(), Movie.NEW_RELEASE);
    }

}
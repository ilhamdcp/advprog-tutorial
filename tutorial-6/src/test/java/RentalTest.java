import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RentalTest {
    private Movie movie;
    private Rental rental;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rental = new Rental(movie, 3);
    }

    // Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Test
    public void getMovie() {
        assertEquals(movie, rental.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rental.getDaysRented());
    }
}
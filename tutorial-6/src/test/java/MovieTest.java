import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

public class MovieTest {
    private Movie movie;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");
        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void testEquals() {
        assertFalse(movie.equals(null));
    }

    @Test
    public void testThisAmount() {
        assertTrue(Math.abs(3.5 - movie.getThisAmount(3)) == 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testError() {
        movie.setPriceCode(666);
    }

    @Test
    public void testHash() {
        assertEquals(movie.hashCode(), movie.hashCode());
    }
}
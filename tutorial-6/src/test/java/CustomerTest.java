import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerTest {
    private Customer customer;
    private Movie movie,childMovie,newMovie;
    private Rental rent,rent1,rent2;

    // Remove redundancy in setting up test fixture in each test methods
    // Hint: Make the test fixture into an instance variable

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        childMovie = new Movie("Pink Panther", Movie.CHILDREN);
        newMovie = new Movie("Infinity Wars", Movie.NEW_RELEASE);
        rent = new Rental(movie, 3);
        rent1 = new Rental(childMovie, 4);
        rent2 = new Rental(newMovie, 2);
    }

    @Test
    public void getName() {
        Customer customer = new Customer("Alice");
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        Rental rent = new Rental(movie, 3);
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent1);
        customer.addRental(rent2);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("Amount owed is 12.5"));
        assertTrue(result.contains("4 frequent renter points"));

    }

    @Test
    public void htmlStatement() {
        customer.addRental(rent);
        customer.addRental(rent1);
        customer.addRental(rent2);

        String result = customer.htmlStatement();
        String[] lines = result.split("\n");

        assertEquals(6, lines.length);
        assertTrue(result.contains("<P>You owe <EM>12.5</EM><P>"));
        assertTrue(result.contains("n this rental you earned <EM>4</EM> frequent renter points<P>"));

    }
}